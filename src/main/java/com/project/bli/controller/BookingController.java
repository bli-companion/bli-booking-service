package com.project.bli.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.project.bli.entity.Booking;
import com.project.bli.entity.BookingResult;
import com.project.bli.service.BookingService;

@RestController
//@RequestMapping("/booking")
public class BookingController {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BookingService bookingService;

	@GetMapping("/all")
	public ResponseEntity<List<Booking>> listAllBookings() {
		List<Booking> list = bookingService.findAllBookings();
		if(list == null) {
			logger.info("No Booking Content");
			return new ResponseEntity<List<Booking>>(HttpStatus.NO_CONTENT);
		}
		else {
			logger.info("Fetching Booking Data");
			return new ResponseEntity<List<Booking>>(list, HttpStatus.OK);
		}
	}

	@GetMapping(value = "/detail/{id}")
	public ResponseEntity<Booking> getBookingById(@PathVariable("id") Integer id) {
		Booking booking = bookingService.findOneBookingById(id);
		if (booking == null) {
			logger.info("Booking with id {} not found.", id);
			return new ResponseEntity<Booking>(HttpStatus.NO_CONTENT);
		}
		else{
			logger.info("Fetching booking with id {}", id);
			return new ResponseEntity<Booking>(booking, HttpStatus.OK);
		}
	}
	
	@GetMapping(value = "/nip/{nip}")
	public ResponseEntity<Booking> getBookingByNip(@PathVariable("nip") String nip) {
		Booking booking = bookingService.findOneBookingByNip(nip);
		if (booking == null) {
			logger.info("Booking with nip {} not found.", nip);
			return new ResponseEntity<Booking>(HttpStatus.NO_CONTENT);
		}
		else{
			logger.info("Fetching booking with nip {}", nip);
			return new ResponseEntity<Booking>(booking, HttpStatus.OK);
		}
	}
	
	@GetMapping(value = "/today/{nip}")
	public ResponseEntity<Boolean> isAlreadyBooked(@PathVariable("nip") String nip) {
		Boolean isBooked = bookingService.isBooked(nip);
		if (isBooked) {
			logger.info("You have already booked");
			return new ResponseEntity<Boolean>(isBooked, HttpStatus.NO_CONTENT);
		}
		else{
			logger.info("You have not booked today");
			return new ResponseEntity<Boolean>(isBooked, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public ResponseEntity<Boolean> createBooking(@RequestBody Booking booking, UriComponentsBuilder ucBuilder) {
		logger.info("Initializing adding new Booking ...");
		boolean flag = bookingService.save(booking);
		if (flag) {
			logger.info("New booking created successfully ...");
			return new ResponseEntity<Boolean>(flag, HttpStatus.CREATED);
		} else {
			logger.info("Failed to create new booking ...");
			return new ResponseEntity<Boolean>(flag, HttpStatus.CONFLICT);
		}
	}
	
	@PutMapping(value = "/confirmation")
	public ResponseEntity<Boolean> updateConfirmation(@RequestBody Booking booking){
		logger.info("Initializing updating Booking Confirmation...");
		boolean flag = bookingService.updateConfirmation(booking);
		if (flag) {
			logger.info("New booking created successfully ...");
			return new ResponseEntity<Boolean>(flag, HttpStatus.CREATED);
		} else {
			logger.info("Failed to create new booking ...");
			return new ResponseEntity<Boolean>(flag, HttpStatus.CONFLICT);
		}
	}
	
	@PutMapping(value = "/rating")
	public ResponseEntity<Boolean> updateRating(@RequestBody Booking booking){
		logger.info("Initializing updating Booking Rating...");
		boolean flag = bookingService.updateRating(booking);
		if (flag) {
			logger.info("New booking created successfully ...");
			return new ResponseEntity<Boolean>(flag, HttpStatus.CREATED);
		} else {
			logger.info("Failed to create new booking ...");
			return new ResponseEntity<Boolean>(flag, HttpStatus.CONFLICT);
		}
	}
	
	//medals/----
	// total book record

		@GetMapping(value = "/rating/average/{stall_id}")
		public ResponseEntity<Double> findAverageRatingByStallId(@PathVariable("stall_id") Integer stall_id) {
			Double averageRating = bookingService.findAverageRatingByStallId(stall_id);
			if(averageRating == null){
				logger.info("Average Rating Booking not found ...");
				return new ResponseEntity<Double>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Average Rating Booking found ...");
				return new ResponseEntity<Double>(averageRating, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/rank/{stall_id}")
		public ResponseEntity<Integer> findStallRank(@PathVariable("stall_id") Integer stall_id) {
			Integer stallRank = bookingService.findStallRank(stall_id);
			if(stallRank == null){
				logger.info("Stall Rank not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Stall Rank Booking found ...");
				return new ResponseEntity<Integer>(stallRank, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/first/{stall_id}")
		public ResponseEntity<String> findFirstRank() {
			String firstRank = bookingService.findFirstRank();
			if(firstRank == null){
				logger.info("First Rank not found ...");
				return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("First Rank Booking found ...");
				return new ResponseEntity<String>(firstRank, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/rare/{nip}")
		public ResponseEntity<Integer> findBadge(@PathVariable String nip) {
			Integer firstRank = bookingService.findRareFoodBadge(nip);
			if(firstRank == null){
				logger.info("Badge not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Badge Booking found ...");
				return new ResponseEntity<Integer>(firstRank, HttpStatus.OK);
			}
		}

		@GetMapping(value = "/total/month/{stall_id}")
		public ResponseEntity<Integer> findTotalBookingThisMonthByStallId(@PathVariable("stall_id") Integer stall_id) {
			Integer totalBookingThisMonth = bookingService.findTotalBookingThisMonthByStallId(stall_id);
			if(totalBookingThisMonth == null){
				logger.info("Total Booking This Month not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Total Booking This Month found ...");
				return new ResponseEntity<Integer>(totalBookingThisMonth, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/total/stall/{stall_id}")
		public ResponseEntity<Integer> findTotalBookingByStallId(@PathVariable("stall_id") Integer stall_id) {
			Integer totalBooking = bookingService.findTotalBookingByStallId(stall_id);
			if(totalBooking == null){
				logger.info("Total Booking not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Total Booking found ...");
				return new ResponseEntity<Integer>(totalBooking, HttpStatus.OK);
			}
		}
		
		//gajalan
		@GetMapping(value = "/total/date/{stall_id}")
		public ResponseEntity<List<BookingResult>> findTotalBookingByDate(@PathVariable Integer stall_id) {
			List<BookingResult> totalBookingByDate = bookingService.findTotalBookingByDate(stall_id);
			if(totalBookingByDate == null){
				logger.info("Total Booking By Date not found ...");
				return new ResponseEntity<List<BookingResult>>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Total Booking found ...");
				return new ResponseEntity<List<BookingResult>>(totalBookingByDate, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/favorite/food/{nip}")
		public ResponseEntity<List<String>> findFavoriteFoodStall(@PathVariable String nip) {
			List<String> favoriteFoodStall = bookingService.findFavoriteFoodStall(nip);
			if(favoriteFoodStall == null){
				logger.info("Favorite Food Stall not found ...");
				return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Favorite Food Stall found ...");
				return new ResponseEntity<List<String>>(favoriteFoodStall, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/least/{nip}")
		public ResponseEntity<List<String>> findVisitUsAgain(@PathVariable String nip) {
			List<String> favoriteFoodStall = bookingService.findVisitUsAgain(nip);
			if(favoriteFoodStall == null){
				logger.info("Favorite Food Stall not found ...");
				return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Favorite Food Stall found ...");
				return new ResponseEntity<List<String>>(favoriteFoodStall, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/trend/picks")
		public ResponseEntity<String> findBliTrendingPick() {
			String trendPicks = bookingService.findBliTrendingPick();
			if(trendPicks == null){
				logger.info("Trend Picks not found ...");
				return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Trend Picks found ...");
				return new ResponseEntity<String>(trendPicks, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/food/enthusiast/{nip}")
		public ResponseEntity<Integer> findFoodEnthusiastBadge(@PathVariable String nip){
			Integer foodEnthusiast = bookingService.findFoodEnthusiastBadge(nip);
			if(foodEnthusiast == null){
				logger.info("Food Enthusiast Badge not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Food Enthusiast Badge found ...");
				return new ResponseEntity<Integer>(foodEnthusiast, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/food/healthy")
		public ResponseEntity<Integer> findHealthyFoodBadge(){
			Integer healthyFood = bookingService.findHealthyFoodBadge();
			if(healthyFood == null){
				logger.info("Healthy Food Badge not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Healthy Food Badge found ...");
				return new ResponseEntity<Integer>(healthyFood, HttpStatus.OK);
			}
		}
		
		@GetMapping(value = "/food/feast")
		public ResponseEntity<Integer> findFeastAtBliBadge(){
			Integer feastFood = bookingService.findFeastAtBliBadge();
			if(feastFood == null){
				logger.info("Feast Food Badge not found ...");
				return new ResponseEntity<Integer>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("Feast Food Badge found ...");
				return new ResponseEntity<Integer>(feastFood, HttpStatus.OK);
			}
		}
		
}