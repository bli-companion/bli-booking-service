package com.project.bli.repository;

import java.util.List;

import com.project.bli.entity.Booking;
import com.project.bli.entity.BookingResult;

public interface BookingDao {
	public List<Booking> findAllBookings();
	public Booking findOneBookingById(Integer id);
	public Booking findOneBookingByNip(String nip);
	public boolean isExists(Integer id);
	public boolean save(Booking booking);
	public boolean updateRating(Booking booking);
	public boolean updateConfirmation(Booking booking);
	public boolean isBooked(String nip);
		
	public Integer findTotalBookAllTime(String id);//gada
	public Double findAverageRatingByStallId(Integer stall_id);
	public Integer findTotalBookingThisMonthByStallId(Integer stall_id);
	public Integer findTotalBookingByStallId(Integer stall_id);
	public Integer findStallRank(Integer stall_id);
	public String findFirstRank();
	public List<BookingResult> findTotalBookingByDate(Integer stall_id);
	public Integer findRareFoodBadge(String nip);
	public List<String> findFavoriteFoodStall(String nip);
	public List<String> findVisitUsAgain(String nip);
	public String findBliTrendingPick();
	public Integer findFoodEnthusiastBadge(String nip);
	public Integer findHealthyFoodBadge();
	public Integer findFeastAtBliBadge();
	
}