package com.project.bli.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.bli.entity.Booking;
import com.project.bli.entity.BookingResult;
import com.project.bli.repository.BookingDao;

@Repository
@Qualifier("BookingDao")
public class BookingDaoImpl implements BookingDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(BookingDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public List<Booking> findAllBookings() {
		String sql = "select booking_id, nip, stall_id from booking where rownum <= 50 order by booking_time desc";
		List<Booking> listData = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Booking.class));
		return listData;
	}

	@Override
	public Booking findOneBookingById(Integer id) {
		List<Booking> listTotalBooking = jdbcTemplate.query("select * from booking where booking_id = '" + id + "'",
				new RowMapper<Booking>() {
					public Booking mapRow(ResultSet rs, int row) throws SQLException {
						Booking booking = new Booking();
						booking.setBooking_id(rs.getInt("booking_id"));
						booking.setNip(rs.getString("nip"));
						booking.setStall_id(rs.getInt("stall_id"));
						booking.setStall_name(rs.getString("stall_name"));
						booking.setStall_content(rs.getString("stall_content"));
						booking.setBooking_time(rs.getTimestamp("booking_time"));
						booking.setRating(rs.getInt("rating"));
						booking.setIs_confirmed(rs.getInt("is_confirmed"));
						return booking;
					}
				});
			return listTotalBooking.get(0);
	}
	
	@Override
	public Booking findOneBookingByNip(String nip) {
		List<Booking> listTotalBooking = jdbcTemplate.query("select * from booking where nip = '" + nip + "' AND TRUNC(BOOKING_TIME) = TRUNC(sysdate)",
				new RowMapper<Booking>() {
					public Booking mapRow(ResultSet rs, int row) throws SQLException {
						Booking booking = new Booking();
						booking.setBooking_id(rs.getInt("booking_id"));
						booking.setNip(rs.getString("nip"));
						booking.setStall_id(rs.getInt("stall_id"));
						booking.setStall_name(rs.getString("stall_name"));
						booking.setStall_content(rs.getString("stall_content"));
						booking.setBooking_time(rs.getTimestamp("booking_time"));
						booking.setRating(rs.getInt("rating"));
						booking.setIs_confirmed(rs.getInt("is_confirmed"));
						return booking;
					}
				});
			return listTotalBooking.get(0);
	}

	@Override
	public boolean isExists(Integer id) {
		String sql = "select count(1) from booking where booking_id = ?";
		Integer count = jdbcTemplate.queryForObject(sql, Integer.class, id);
		return count != null && count > 0;
	}
	
	@Override
	public boolean isBooked(String nip) {
		String sql = "select count(*) from booking where TRUNC(BOOKING_TIME) = TRUNC(sysdate) and nip = '"+nip+"'";
		Integer count = jdbcTemplate.queryForObject(sql, Integer.class);
		return count != null && count > 0;
	}
	
	@Override
	public boolean save(Booking booking) {
		String sql = "insert into booking (nip, stall_id, booking_time, stall_name, stall_content) values (?, ?, sysdate, ?, ?)";
		int rowAffected = jdbcTemplate.update(sql, booking.getNip(), booking.getStall_id(), booking.getStall_name(), booking.getStall_content());
		LOGGER.info("Checking affected row ...");
		if (rowAffected > 0) {
			LOGGER.info("New class successfully inserted ...");
			return true;
		}
		LOGGER.info("Failed to insert new class ...");
		return false;
	}
	
	@Override
	public boolean updateConfirmation(Booking booking){
		String sql = "update booking set is_confirmed = ? where booking_id = ?";
		int rowAffected = jdbcTemplate.update(sql, booking.getIs_confirmed(), booking.getBooking_id());
		LOGGER.info("Checking affected row ...");
		if (rowAffected > 0) {
			LOGGER.info("New class successfully inserted ...");
			return true;
		}
		LOGGER.info("Failed to insert new class ...");
		return false;
	}
	
	@Override
	public boolean updateRating(Booking booking){
		String sql = "update booking set rating = ? where booking_id = ?";
		int rowAffected = jdbcTemplate.update(sql, booking.getRating(), booking.getBooking_id());
		LOGGER.info("Checking affected row ...");
		if (rowAffected > 0) {
			LOGGER.info("New class successfully inserted ...");
			return true;
		}
		LOGGER.info("Failed to insert new class ...");
		return false;
	}
	
	//medals//--------------------------------
	
	@Override
	public List<BookingResult> findTotalBookingByDate(Integer stall_id) {
		List<BookingResult> listBookingResult = jdbcTemplate.query("select substr(booking_time,1,9) as dt, "
				+ "count(*) as total_book, stall_name from booking where stall_id = "+stall_id
				+ " group by substr(booking_time,1,9), stall_name order by substr(booking_time,1,9) desc,stall_name",
				new RowMapper<BookingResult>() {
					public BookingResult mapRow(ResultSet rs, int row) throws SQLException {
						BookingResult booking = new BookingResult();
						System.out.println("BOOKINGNYA: "+booking.toString());
						booking.setDt(rs.getString("dt"));
						booking.setTotal_book(rs.getInt("total_book"));
						booking.setStall_name(rs.getString("stall_name"));
						System.out.println(booking.toString());
						return booking;
					}
				});
			return listBookingResult;
	}
	
	@Override
	public Integer findTotalBookingByStallId(Integer stall_id) {
		List<Integer> listTotalBooking = jdbcTemplate.query("select count(*) as total from booking "
				+ "where stall_id = "+stall_id+" group by stall_name order by stall_name",
			new RowMapper<Integer>() {
				public Integer mapRow(ResultSet rs, int row) throws SQLException {
					return rs.getInt(1);
				}
			});
		return listTotalBooking.get(0);
	}
	
	@Override
	public Integer findTotalBookingThisMonthByStallId(Integer stall_id) {
		List<Integer> listTotalBooking = jdbcTemplate.query("select count(*) as total from booking "
				+ "where to_char(booking_time,'j') between to_char(add_months(last_day(sysdate),-1),'j')+1 "
				+ "and to_char(last_day(sysdate),'j') and stall_id = "+stall_id+" group by stall_name "
				+ "order by stall_name", new RowMapper<Integer>() {
				public Integer mapRow(ResultSet rs, int row) throws SQLException {
					return rs.getInt(1);
				}
			});
		return listTotalBooking.get(0);
	}

	@Override
	public Double findAverageRatingByStallId(Integer stall_id){
		List<Double> listTotalBooking = jdbcTemplate.query("select round(avg(rating),2) as rating from booking "
				+ "where to_char(booking_time,'j') between to_char(sysdate,'j')-30 "
				+ "and to_char(sysdate,'j') and stall_id = "+stall_id+" group by stall_name",
				new RowMapper<Double>() {
					public Double mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getDouble(1);
					}
				});
			return listTotalBooking.get(0);
	}
	
	@Override
	public Integer findTotalBookAllTime(String id) {
		List<Integer> totalBookAllTime = jdbcTemplate.query(
				"select COUNT(EXTRACT(MONTH from booking_time)) " + "from booking where stall_id = '" + id + "'",
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getInt(1);
					}
				});
		return totalBookAllTime.get(0);
	}
	
	@Override
	public Integer findStallRank(Integer stall_id) {
		List<Integer> stallRank = jdbcTemplate.query(
				"select rowno from (select total_book.*, row_number () over ( order by times DESC ) AS RowNo "
				+ "from (select stall_id, stall_name, count(booking_id) as times from booking "
				+ "group by stall_id, stall_name) total_book) where stall_id = "+stall_id,
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getInt(1);
					}
				});
		return stallRank.get(0);
	}
	
	@Override
	public String findFirstRank(){
		List<String> firstRank = jdbcTemplate.query(
				"select stall_name from (select total_book.*, row_number () over ( order by times DESC ) AS RowNo "
				+ "from (select stall_id, stall_name, count(booking_id) as times from booking "
				+ "group by stall_id, stall_name) total_book) where rowno = 1",
				new RowMapper<String>() {
					public String mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getString(1);
					}
				});
		return firstRank.get(0);
	}
	
	@Override
	public Integer findRareFoodBadge(String nip){
		List<Integer> badge = jdbcTemplate.query(
				"SELECT "
				+ "CASE WHEN gold >= 5 THEN 1 ELSE "
				+ "CASE WHEN silver >= 5 THEN 2 ELSE "
				+ "CASE WHEN bronze >= 5 THEN 3 ELSE 0 "
				+ "END "
				+ "END "
				+ "END AS badge "
				+ "FROM"
				+ " (SELECT nip,"
				+ "SUM(flag_bronze) AS bronze,"
				+ "SUM(flag_silver) AS silver,"
				+ "SUM(flag_gold)   AS gold "
				+ "FROM"
				+ "(SELECT a.*,"
				+ "CASE WHEN times >= 1 AND times < 5 THEN 1 ELSE 0 END AS flag_bronze,"
				+ "CASE WHEN times >= 5 AND times   < 10 THEN 1 ELSE 0 END AS flag_silver,"
				+ "CASE WHEN times >= 10 THEN 1 ELSE 0 END AS flag_gold "
				+ "FROM"
				+ "(SELECT nip, stall_name, COUNT(stall_name) AS times "
				+ "FROM booking "
				+ "where nip = '"+ nip + "' "
				+ "GROUP BY nip, stall_name "
				+ "ORDER BY nip"
				+ ")a"
				+ ")"
				+ "GROUP BY nip "
				+ "ORDER BY nip)",
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getInt(1);
					}
				});
		return badge.get(0);
	}
	
	@Override
	public List<String> findFavoriteFoodStall(String nip){
		List<String> favoriteFoodStall = jdbcTemplate.query(
				"select stall_name from ("
				+ "select a.*, row_number () over ("
				+ "PARTITION BY a.nip "
				+ "order by a.times desc"
				+ ") AS RowNo "
				+ "from("
				+ "SELECT nip ,"
				+ "COUNT(booking_id) AS times,"
				+ "stall_name "
				+ "FROM booking "
				+ "GROUP BY nip,"
				+ "stall_name "
				+ "ORDER BY nip) a "
				+ "order by nip, times desc) a "
				+ "where rowno <= 3 and nip = '"+nip+"' ",
				new RowMapper<String>() {
					public String mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getString(1);
					}
				});
		return favoriteFoodStall;
	}
	
	@Override
	public List<String> findVisitUsAgain(String nip){
		List<String> visitUsAgain = jdbcTemplate.query(
				"select stall_name "
				+ "from ("
				+ "select a.*, row_number () over ("
				+ "PARTITION BY a.nip "
				+ "order by a.times"
				+ ") AS RowNo "
				+ "from("
				+ "SELECT nip ,"
				+ "COUNT(booking_id) AS times,"
				+ "stall_name "
				+ "FROM booking "
				+ "GROUP BY nip,"
				+ "stall_name "
				+ "ORDER BY nip) a "
				+ "order by nip, times) a "
				+ "where rowno <= 3 and nip = '"+nip+"' ",
				new RowMapper<String>() {
					public String mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getString(1);
					}
				});
		return visitUsAgain;
	}
	
	@Override
	public String findBliTrendingPick(){
		List<String> bliTrendingPick = jdbcTemplate.query(
				"select stall_name "
				+ "from ("
				+ "select a.*, row_number () over ("
				+ "order by a.total_book desc "
				+ ") AS RowNo "
				+ "from (select stall_name, count(*) as total_book "
				+ "from booking "
				+ "where to_char(booking_time,'j') between 2458401-30 and 2458401 "
				+ "group by stall_name) a) "
				+ "where rowno = 1",
				new RowMapper<String>() {
					public String mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getString(1);
					}
				});
		return bliTrendingPick.get(0);
	}
	
	@Override
	public Integer findFoodEnthusiastBadge(String nip){
		List<Integer> foodEnthusiast = jdbcTemplate.query(
				"SELECT "
				+ "CASE WHEN gold >= 7 THEN 1 ELSE "
				+ "CASE WHEN silver >= 7 THEN 2 ELSE "
				+ "CASE WHEN bronze >= 7 THEN 3 ELSE 0 "
				+ "END "
				+ "END "
				+ "END AS badge "
				+ "FROM "
				+ "(SELECT nip,"
				+ "SUM(flag_bronze) AS bronze,"
				+ "SUM(flag_silver) AS silver,"
				+ "SUM(flag_gold)   AS gold "
				+ "FROM "
				+ "(SELECT a.*,"
				+ "CASE WHEN times >= 5 AND times < 10 THEN 1 ELSE 0 END AS flag_bronze,"
				+ "CASE WHEN times >= 10 AND times   < 15 THEN 1 ELSE 0 END AS flag_silver,"
				+ "CASE WHEN times >= 15 THEN 1 ELSE 0 END AS flag_gold "
				+ "FROM"
				+ "(SELECT nip, stall_name, COUNT(stall_name) AS times "
				+ "FROM booking "
				+ "GROUP BY nip, stall_name "
				+ "ORDER BY nip "
				+ ") a"
				+ ")"
				+ "GROUP BY nip "
				+ "ORDER BY nip"
				+ ")"
				+ "WHERE nip = '"+nip+"'",
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getInt(1);
					}
				});
		return foodEnthusiast.get(0);
	}
	
	@Override
	public Integer findHealthyFoodBadge(){
		List<Integer> healthyFood = jdbcTemplate.query(
				"select "
				+ "case when flag_bronze = 1 then 3 else "
				+ "case when flag_silver = 1 then 2 else "
				+ "case when flag_gold = 1 then 1 else 0 "
				+ "end "
				+ "end "
				+ "end as badge "
				+ "from("
				+ "select nip,"
				+ "case when check_menu_diet >= 5 and check_menu_diet < 10 then 1 else 0 end flag_bronze,"
				+ "case when check_menu_diet >= 10 and check_menu_diet < 15 then 1 else 0 end flag_silver,"
				+ "case when check_menu_diet >= 15 then 1 else 0 end flag_gold "
				+ "from ("
				+ "select nip,"
				+ "sum(case when stall_name = 'MENU DIET' then 1 else 0 end) as check_menu_diet "
				+ "from booking "
				+ "group by nip "
				+ "order by nip))",
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getInt(1);
					}
				});
		return healthyFood.get(0);
	}
	
	@Override
	public Integer findFeastAtBliBadge(){
		List<Integer> feastAtBli = jdbcTemplate.query(
				"select case when flag_bronze = 1 then 3 else case when flag_silver = 1 then 2 else case when flag_gold = 1 then 1 else 0 end end end as badge "
				+ "from("
				+ "select nip,"
				+ "case when check_PRASMANAN >= 5 and check_PRASMANAN < 10 then 1 else 0 end flag_bronze,"
				+ "case when check_PRASMANAN >= 10 and check_PRASMANAN < 15 then 1 else 0 end flag_silver,"
				+ "case when check_PRASMANAN >= 15 then 1 else 0 end flag_gold from ("
				+ "select nip,"
				+ "sum(case when stall_name = 'PRASMANAN' then 1 else 0 end) as check_PRASMANAN "
				+ "from booking "
				+ "group by nip "
				+ "order by nip))",
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int row) throws SQLException {
						return rs.getInt(1);
					}
				});
		return feastAtBli.get(0);
	}
	
}