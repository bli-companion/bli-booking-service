/*package com.project.bli;


import java.sql.SQLException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.SqlConfig;

import oracle.jdbc.pool.OracleDataSource;

@Configuration
@ComponentScan(basePackages="Booking")
@SqlConfig
public class SpringJDBCConfiguration {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private Environment env;
	
	@Bean
	public DataSource dataSource() throws SQLException {
		logger.info("run : " + env.getProperty("booking.db.mst.url"));
		OracleDataSource dataSource = new OracleDataSource();
		dataSource.setURL(env.getProperty("booking.db.mst.url"));
		dataSource.setUser(env.getProperty("booking.db.mst.username"));
		dataSource.setPassword(env.getProperty("booking.db.mst.password"));
		return dataSource;
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}
}*/