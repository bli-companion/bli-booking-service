package com.project.bli.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.project.bli.entity.Booking;
import com.project.bli.entity.BookingResult;
import com.project.bli.entity.Staff;
import com.project.bli.repository.BookingDao;

@Service
public class BookingService {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	BookingDao bookingDao;
	private Staff staff = new Staff();

	public List<Booking> findAllBookings() {
		return bookingDao.findAllBookings();
	}

	public Booking findOneBookingById(Integer id) {
		return bookingDao.findOneBookingById(id);
	}
	
	public Booking findOneBookingByNip(String nip) {
		return bookingDao.findOneBookingByNip(nip);
	}

	public boolean isExists(Integer id) {
		return bookingDao.isExists(id);
	}
	
	public boolean isBooked(String nip){
		return bookingDao.isBooked(nip);
	}

	public boolean save(Booking booking) {
		logger.info("Add booking service triggered!");
		if(isExists(booking.getBooking_id())) return false;
		bookingDao.save(booking);
		logger.info("Add booking potentially successful ...");
		return true;
	}
	
	public boolean updateConfirmation(Booking booking) {
		return bookingDao.updateConfirmation(booking);
	}
	
	public boolean updateRating(Booking booking) {
		return bookingDao.updateRating(booking);
	}
	
	public Double findAverageRatingByStallId(Integer stall_id){
		return bookingDao.findAverageRatingByStallId(stall_id);
	}
	
	public Integer findTotalBookingThisMonthByStallId(Integer stall_id){
		return bookingDao.findTotalBookingThisMonthByStallId(stall_id);
	}
	
	public Integer findTotalBookingByStallId(Integer stall_id){
		return bookingDao.findTotalBookingByStallId(stall_id);
	}
	
	public List<BookingResult> findTotalBookingByDate(Integer stall_id){
		return bookingDao.findTotalBookingByDate(stall_id);
	}
	
	public Integer findStallRank(Integer stall_id){
		return bookingDao.findStallRank(stall_id);
	}
	
	public String findFirstRank(){
		return bookingDao.findFirstRank();
	}
	
	public Integer findRareFoodBadge(String nip){
		return bookingDao.findRareFoodBadge(nip);
	}
	
	public List<String> findFavoriteFoodStall(String nip){
		return bookingDao.findFavoriteFoodStall(nip);
	}
	
	public List<String> findVisitUsAgain(String nip){
		return bookingDao.findVisitUsAgain(nip);
	}
	
	public String findBliTrendingPick(){
		return bookingDao.findBliTrendingPick();
	}
	
	public Integer findFoodEnthusiastBadge(String nip){
		return bookingDao.findFoodEnthusiastBadge(nip);
	}
	
	public Integer findHealthyFoodBadge(){
		return bookingDao.findHealthyFoodBadge();
	}
	
	public Integer findFeastAtBliBadge(){
		return bookingDao.findFeastAtBliBadge();
	}
	@KafkaListener(topics = "log-in-booking", groupId = "consumer")
	public void consume(Staff staff)
	{
		System.out.println("consumed staff" + staff);
		setStaff(staff);
	}
	
	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}
	
}