package com.project.bli.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.Nullable;

@Entity
@Table(name="stall")
public class Stall {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="STALL_SEQ")
	@SequenceGenerator(sequenceName="stall_seq", allocationSize=1, name="STALL_SEQ")
	private int stall_id;
	
	@NotNull
	private String location;
	
	@Nullable
	private int book_stock;
	
	@Nullable
	private int queue_stock;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "food_id", nullable = false)
	private Food food;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@NotNull
	private String created_by;
	
	@Column(nullable=true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@Nullable
	private String updated_by;
	
	@NotNull
	private boolean is_deleted = false;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "vendor_staff_nip", nullable = false)
	private Vendor vendor;
	
	public Stall() {}

	public Stall(int stall_id, @NotNull String location, int book_stock, int queue_stock, Food food, Date created_at,
			@NotNull String created_by, Date updated_at, String updated_by, @NotNull boolean is_deleted,
			Vendor vendor) {
		super();
		this.stall_id = stall_id;
		this.location = location;
		this.book_stock = book_stock;
		this.queue_stock = queue_stock;
		this.food = food;
		this.created_at = created_at;
		this.created_by = created_by;
		this.updated_at = updated_at;
		this.updated_by = updated_by;
		this.is_deleted = is_deleted;
		this.vendor = vendor;
	}

	public int getId() {
		return stall_id;
	}

	public void setId(int stall_id) {
		this.stall_id = stall_id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getBook_stock() {
		return book_stock;
	}

	public void setBook_stock(int book_stock) {
		this.book_stock = book_stock;
	}

	public int getQueue_stock() {
		return queue_stock;
	}

	public void setQueue_stock(int queue_stock) {
		this.queue_stock = queue_stock;
	}

	public Food getFood() {
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public boolean isIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Stall [stall_id=");
		builder.append(stall_id);
		builder.append(", location=");
		builder.append(location);
		builder.append(", book_stock=");
		builder.append(book_stock);
		builder.append(", queue_stock=");
		builder.append(queue_stock);
		builder.append(", food=");
		builder.append(food);
		builder.append(", created_at=");
		builder.append(created_at);
		builder.append(", created_by=");
		builder.append(created_by);
		builder.append(", updated_at=");
		builder.append(updated_at);
		builder.append(", updated_by=");
		builder.append(updated_by);
		builder.append(", is_deleted=");
		builder.append(is_deleted);
		builder.append(", vendor=");
		builder.append(vendor);
		builder.append("]");
		return builder.toString();
	}
}