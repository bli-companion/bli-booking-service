package com.project.bli.entity;

import java.util.Date;

public class BookingResult {
	String dt;
	Integer total_book;
	String stall_name;
	
	public BookingResult() {}

	public BookingResult(String dt, Integer total_book, String stall_name) {
		super();
		this.dt = dt;
		this.total_book = total_book;
		this.stall_name = stall_name;
	}

	public String getDt() {
		return dt;
	}

	public void setDt(String dt) {
		this.dt = dt;
	}

	public Integer getTotal_book() {
		return total_book;
	}

	public void setTotal_book(Integer total_book) {
		this.total_book = total_book;
	}

	public String getStall_name() {
		return stall_name;
	}

	public void setStall_name(String stall_name) {
		this.stall_name = stall_name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BookingResult [dt=");
		builder.append(dt);
		builder.append(", total_book=");
		builder.append(total_book);
		builder.append(", stall_name=");
		builder.append(stall_name);
		builder.append("]");
		return builder.toString();
	}
	
}
