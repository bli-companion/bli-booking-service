package com.project.bli.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "vendor")
public class Vendor {

	@Id
	@NotNull
	private String vendor_staff_nip;
	
	@Nullable
	private String address;
	
	@Nullable
	private String email;

	@Nullable
	private String phone;
	
	@Nullable
	private Date start_date;
	
	@Nullable
	private Date end_date;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@NotNull
	private String created_by;
	
	@Column(nullable=true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@Nullable
	private String updated_by;
	
	public Vendor() {
		
	}

	public Vendor(@NotNull String vendor_staff_nip, String address, String email, String phone, Date start_date,
			Date end_date, Date created_at, @NotNull String created_by, Date updated_at, String updated_by) {
		super();
		this.vendor_staff_nip = vendor_staff_nip;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.start_date = start_date;
		this.end_date = end_date;
		this.created_at = created_at;
		this.created_by = created_by;
		this.updated_at = updated_at;
		this.updated_by = updated_by;
	}

	public String getVendor_staff_nip() {
		return vendor_staff_nip;
	}

	public void setVendor_staff_nip(String vendor_staff_nip) {
		this.vendor_staff_nip = vendor_staff_nip;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Vendor [vendor_staff_nip=");
		builder.append(vendor_staff_nip);
		builder.append(", address=");
		builder.append(address);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", start_date=");
		builder.append(start_date);
		builder.append(", end_date=");
		builder.append(end_date);
		builder.append(", created_at=");
		builder.append(created_at);
		builder.append(", created_by=");
		builder.append(created_by);
		builder.append(", updated_at=");
		builder.append(updated_at);
		builder.append(", updated_by=");
		builder.append(updated_by);
		builder.append("]");
		return builder.toString();
	}
	
}
