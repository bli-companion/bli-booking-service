package com.project.bli.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "division")
public class Division implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="DIVISION_SEQ")
	@SequenceGenerator(sequenceName="division_seq", allocationSize=1, name="DIVISION_SEQ")
	private Integer id;
	
	@NotNull
	@Size(max=100)
	private String name;
	
	public Division() {
		
	}
	
	public Division(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Division [id=" + id + ", name=" + name + "]";
	}
	
}
