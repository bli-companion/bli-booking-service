package com.project.bli.entity;

public enum Role {
	ROLE_BO,
	ROLE_ADMIN,
	ROLE_VENDOR,
	ROLE_STAFF
}
