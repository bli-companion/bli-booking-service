package com.project.bli.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="food")
public class Food {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FOOD_SEQ")
	@SequenceGenerator(sequenceName="food_seq", allocationSize=1, name="FOOD_SEQ")
	private int food_id;
	
	@NotNull
	private String name;
	
	@NotNull
	private String description;
	
	@NotNull
	private int reset_book_stock;
	
	@NotNull
	private int reset_queue_stock;
	
	@Nullable
	private int soldout_counter;
	
	@Nullable
	private int soldout_booking_time;
	
	@NotNull
	private boolean is_deleted = false;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@NotNull
	private String created_by;
	
	@Column(nullable=true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@Nullable
	private String updated_by;

	public Food(){}

	public Food(int food_id, @NotNull String name, @NotNull String description, @NotNull int reset_book_stock,
			@NotNull int reset_queue_stock, int soldout_counter, int soldout_booking_time, @NotNull boolean is_deleted,
			Date created_at, @NotNull String created_by, Date updated_at, String updated_by) {
		super();
		this.food_id = food_id;
		this.name = name;
		this.description = description;
		this.reset_book_stock = reset_book_stock;
		this.reset_queue_stock = reset_queue_stock;
		this.soldout_counter = soldout_counter;
		this.soldout_booking_time = soldout_booking_time;
		this.is_deleted = is_deleted;
		this.created_at = created_at;
		this.created_by = created_by;
		this.updated_at = updated_at;
		this.updated_by = updated_by;
	}

	public int getId() {
		return food_id;
	}

	public void setId(int food_id) {
		this.food_id = food_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getReset_book_stock() {
		return reset_book_stock;
	}

	public void setReset_book_stock(int reset_book_stock) {
		this.reset_book_stock = reset_book_stock;
	}

	public int getReset_queue_stock() {
		return reset_queue_stock;
	}

	public void setReset_queue_stock(int reset_queue_stock) {
		this.reset_queue_stock = reset_queue_stock;
	}

	public int getSoldout_counter() {
		return soldout_counter;
	}

	public void setSoldout_counter(int soldout_counter) {
		this.soldout_counter = soldout_counter;
	}

	public int getSoldout_booking_time() {
		return soldout_booking_time;
	}

	public void setSoldout_booking_time(int soldout_booking_time) {
		this.soldout_booking_time = soldout_booking_time;
	}

	public boolean isIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Food [food_id=");
		builder.append(food_id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", reset_book_stock=");
		builder.append(reset_book_stock);
		builder.append(", reset_queue_stock=");
		builder.append(reset_queue_stock);
		builder.append(", soldout_counter=");
		builder.append(soldout_counter);
		builder.append(", soldout_booking_time=");
		builder.append(soldout_booking_time);
		builder.append(", is_deleted=");
		builder.append(is_deleted);
		builder.append(", created_at=");
		builder.append(created_at);
		builder.append(", created_by=");
		builder.append(created_by);
		builder.append(", updated_at=");
		builder.append(updated_at);
		builder.append(", updated_by=");
		builder.append(updated_by);
		builder.append("]");
		return builder.toString();
	}
}