package com.project.bli.entity;

import java.sql.Timestamp;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

public class Booking {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BOOKING_SEQ")
	@SequenceGenerator(sequenceName="booking_seq", allocationSize=1, name="BOOKING_SEQ")
	private Integer booking_id;
	@NotNull
	private String nip;
	@NotNull
	private Integer stall_id;
	@NotNull
	private String stall_name;
	@NotNull
	private String stall_content;
	@NotNull
	private Timestamp booking_time;
	@Nullable
	private Integer rating;
	@Nullable
	private Integer is_confirmed;
	
	public Booking(){}

	public Booking(Integer booking_id, String nip, Integer stall_id, String stall_name, String stall_content,
			Timestamp booking_time, Integer rating, Integer is_confirmed) {
		super();
		this.booking_id = booking_id;
		this.nip = nip;
		this.stall_id = stall_id;
		this.stall_name = stall_name;
		this.stall_content = stall_content;
		this.booking_time = booking_time;
		this.rating = rating;
		this.is_confirmed = is_confirmed;
	}

	public Integer getBooking_id() {
		return booking_id;
	}

	public void setBooking_id(Integer booking_id) {
		this.booking_id = booking_id;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public Integer getStall_id() {
		return stall_id;
	}

	public void setStall_id(Integer stall_id) {
		this.stall_id = stall_id;
	}

	public String getStall_name() {
		return stall_name;
	}

	public void setStall_name(String stall_name) {
		this.stall_name = stall_name;
	}

	public String getStall_content() {
		return stall_content;
	}

	public void setStall_content(String stall_content) {
		this.stall_content = stall_content;
	}

	public Timestamp getBooking_time() {
		return booking_time;
	}

	public void setBooking_time(Timestamp booking_time) {
		this.booking_time = booking_time;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Integer getIs_confirmed() {
		return is_confirmed;
	}

	public void setIs_confirmed(Integer is_confirmed) {
		this.is_confirmed = is_confirmed;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Booking [booking_id=");
		builder.append(booking_id);
		builder.append(", nip=");
		builder.append(nip);
		builder.append(", stall_id=");
		builder.append(stall_id);
		builder.append(", stall_name=");
		builder.append(stall_name);
		builder.append(", stall_content=");
		builder.append(stall_content);
		builder.append(", booking_time=");
		builder.append(booking_time);
		builder.append(", rating=");
		builder.append(rating);
		builder.append(", is_confirmed=");
		builder.append(is_confirmed);
		builder.append("]");
		return builder.toString();
	}
}